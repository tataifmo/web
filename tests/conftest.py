import os
import pytest
import datetime
import web
import tempfile
import sqlite3
from flask import current_app, g
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import flask_login

from app.models import User


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))


@pytest.fixture(scope='module')
def new_user():
    user = User(login='Max', email='maximilian@mail.ru')
    return user


@pytest.fixture(scope='module')
def new_user_age():
    year = 1995
    month = 8
    day = 31
    birthday = datetime.datetime(year, month, day)
    user = User(login='Max', email='maximilian@mail.ru', birth_date=birthday)
    return user


@pytest.fixture(scope='session')
def app():
    flask_app = Flask(__name__)
    flask_app.config['TESTING'] = True
    flask_app.config['WTF_CSRF_ENABLED'] = False
    return flask_app


@pytest.fixture(scope='session')
def test_client():
    db_fd, web.app.config['DATABASE'] = tempfile.mkstemp()
    web.app.config['TESTING'] = True

    testing_client = web.app.test_client()
    with web.app.app_context():
        init_db()

    yield testing_client
    os.close(db_fd)
    os.unlink(web.app.config['DATABASE'])
    # ctx.pop()


@pytest.fixture(scope='session')
def init_database():
    db, web.app.config['DATABASE'] = tempfile.mkstemp()
    # Create the database and the database table
    db.create_all()

    # Insert user data
    user1 = User(login='Max3', email='maximilian1@mail.ru')
    user2 = User(login='Max4', email='maximilian2@mail.ru', password='PaSsWoRd')
    db.session.add(user1)
    db.session.add(user2)

    # Commit the changes for the users
    db.session.commit()

    yield db  # this is where the testing happens!

    db.drop_all()
