from datetime import datetime
'''
Реализация юнит-тестов для методов:
- регистрация пользователя(проверяется создание пользователя через conftest.py)
- реализация проверки работы функции login невозможна для юнит-тестов, потому что использовался встроенный flask модуль
- генерация пароля сравнивается со значением в поле хеш-пароля (сгенерированный пароль состоит из логина+дня создания)
- подсчет возраста user(в conftest.py создается новый пользователь с определенной датой рождения (23 года))
'''


def test_registration(new_user):
    assert new_user.email == 'maximilian@mail.ru'
    assert new_user.login == 'Max'


def test_generate_pass(new_user):
    day = str(new_user.created_at.day)
    assert new_user.password_hash == new_user.generate_pass()
    assert new_user.generate_pass() == 'Max' + day


def test_get_age(new_user_age):
    assert new_user_age.get_age(new_user_age.birth_date) == 23
