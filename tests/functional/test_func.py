def test_registration(test_client):

    response = test_client.post('/register', data={
        'login': 'Maxim',
        'email': 'maxim@mail.ru',
        'password': 'PaSsWoRd'
    })
    assert response.status_code == 200
    assert b'profile' in response.data
    assert b'logout' in response.data


def test_login(test_client, init_database):

    response = test_client.post('/login', data={
        'login': 'Max',
        'password': 'PaSsWoRd'
    })
    assert response.status_code == 200
    assert b'profile' in response.data


def test_update_profile(test_client, init_database):
    response = test_client.post('/login', data={
        'login': 'Max',
        'password': 'PaSsWoRd'
    }, follow_redirect=True)
    response = test_client.post('/update', data={'login': 'Alex'}, follow_redirect=True)
    assert response.status_code == 200
    assert b'Alex' in response.data
