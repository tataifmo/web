import datetime
import hashlib

from flask_login import UserMixin

from app import db, login


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    fname = db.Column(db.String(64))
    name = db.Column(db.String(64))
    lname = db.Column(db.String(64))
    birth_date = db.Column(db.Date)
    is_blocked = db.Column(db.Boolean, default=False)
    auth_key = db.Column(db.String(120), index=True, unique=True)

    def __init__(self, login, email, password=None, fname=None, name=None, lname=None, is_blocked=False, birth_date=None):
        self.login = login
        self.email = email
        self.created_at = datetime.datetime.utcnow()
        self.updated_at = datetime.datetime.utcnow()
        if password is None:
            self.password_hash = self.generate_pass()
        else:
            self.password_hash = self.set_password(password)
        self.fname = fname
        self.name = name
        self.lname = lname
        self.is_blocked = is_blocked
        self.auth_key = self.set_password(self.email)
        self.birth_date = birth_date

    def set_password(self, password):
        return hashlib.md5(password.encode('utf-8')).hexdigest()

    def check_password(self, password):
        if hashlib.md5(password.encode('utf-8')).hexdigest() == self.password_hash:
            return True
        else:
            return False

    def check_authkey(self, auth_key):
        if auth_key == self.auth_key:
            return True
        else:
            return False

    def dateformat(self, dat):
        return dat.strftime('%A, %d. %B %Y %I:%M%p')

    def get_full_name(self):
        fname = self.fname
        name = self.name
        lname = self.lname
        return '{} {} {}'.format(fname, name, lname)

    def get_age(self, born):
        today = datetime.date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    def update_user(self):
        pass

    def password_recovery(self, email):
        pass

    def get_short_name(self):
        fname = self.fname
        name = self.name[0]
        lname = self.lname[0]
        return '{} {}.{}.'.format(fname, name, lname)

    def generate_pass(self):
        key = self.login + str(self.created_at.day)
        return key


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
