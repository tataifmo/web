from urllib.parse import urlparse
import datetime

from flask import request
from flask import render_template, flash, redirect, url_for
from flask_login import current_user, login_user, logout_user, login_required

from app import app, db
from app.forms import LoginForm, RegistrationForm, PassrecoveryForm, UpdateForm
from app.models import User


@app.route('/')
#@login_required
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        if form.auth_key.data:
            user = User.query.filter_by(auth_key=form.auth_key.data).first()
            if user is None or not user.check_authkey(form.auth_key.data):
                flash('Невалидный auth_key')
                return redirect(url_for('login'))
        else:
            user = User.query.filter_by(login=form.login.data).first()
            if user is None or not user.check_password(form.password.data):
                flash('Невалидный login или password')
                return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or urlparse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/profile')
def profile():
    return render_template('profile.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    data = {'1': 'January', '2': 'February', '3': 'March', '4': 'April','5': 'May', '6': 'June', '7': 'July', '8': 'August', '9': 'September', '10': 'October', '11': 'November', '12': 'December'}
    if form.validate_on_submit():
        if form.b_day.data and form.b_year.data:
            day = int(form.b_day.data)
            select = request.form.get('comp_select')
            month = int(str(select))
            year = int(form.b_year.data)
            birthday = datetime.datetime(year, month, day)
        else:
            birthday = None
        user = User(login=form.login.data, email=form.email.data, password=form.password.data, fname=form.fname.data, name=form.name.data, lname=form.lname.data,
                    birth_date=birthday)
        db.session.add(user)
        db.session.commit()
        flash('Ваш аккаунт зарегистрирован')
        login_user(user)
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form, data=data)


@app.route('/update', methods=['GET', 'POST'])
def update():
    data = {'1': 'January', '2': 'February', '3': 'March', '4': 'April', '5': 'May', '6': 'June', '7': 'July',
            '8': 'August', '9': 'September', '10': 'October', '11': 'November', '12': 'December'}
    form = UpdateForm()
    if current_user.is_authenticated:
        if form.validate_on_submit():
            id = current_user.id
            user = User.query.filter_by(id=id).first()
            if form.login.data:
                user.login = form.login.data
            if form.email.data:
                user.email = form.email.data
            if form.password.data:
                user.password_hash = user.set_password(form.password.data)
            if form.fname.data:
                user.fname = form.fname.data
            if form.name.data:
                user.name = form.name.data
            if form.lname.data:
                user.lname = form.lname.data
            if form.b_day.data and form.b_year.data:
                day = int(form.b_day.data)
                select = request.form.get('comp_select')
                month = int(str(select))
                year = int(form.b_year.data)
                birthday = datetime.datetime(year, month, day)
                user.birth_date = birthday
            user.updated_at = datetime.datetime.utcnow()
            db.session.commit()
            flash('Данные обновлены')
            return redirect(url_for('profile'))
        return render_template('updatedata.html', title='Update', form=form, data=data)


@app.route('/passrecovery', methods=['GET', 'POST'])
def passrecovery():
    form = PassrecoveryForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        user.password_hash = user.set_password(form.password.data)
        db.session.commit()
        flash('Ваш пароль обновлен')
        return redirect(url_for('login'))
    return render_template('passrecovery.html', title='Fogot', form=form)


@app.route('/delete')
def delete():
    id = current_user.id
    User.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('index'))

