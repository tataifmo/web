DROP TABLE IF EXISTS user;

CREATE TABLE user (
id INTEGER NOT NULL,
login VARCHAR(64),
email VARCHAR(120),
password_hash VARCHAR(128),
created_at DATETIME,
updated_at DATETIME,
fname VARCHAR(64),
name VARCHAR(64),
lname VARCHAR(64),
birth_date DATE,
is_blocked BOOLEAN,
auth_key VARCHAR(120),
PRIMARY KEY (id),
CHECK (is_blocked IN (0, 1)) );