from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, DateTimeField, FieldList, FormField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo

from app.models import User


class LoginForm(FlaskForm):
    login = StringField('Login')
    password = PasswordField('Password')
    auth_key = StringField('Auth_key')
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    login = StringField('Login:', validators=[DataRequired()])
    email = StringField('Email:', validators=[DataRequired(), Email()])
    password = PasswordField('Password:')
    fname = StringField('Surname:')
    name = StringField('Name:')
    lname = StringField('Lname:')
    b_day = StringField('Day')
    b_month = StringField('Month')
    b_year = StringField('Year')
    submit = SubmitField('Registration')

    def validate_login(self, login):
        user = User.query.filter_by(login=login.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class UpdateForm(FlaskForm):
    login = StringField('Login:')
    email = StringField('Email:')
    password = PasswordField('Password:')
    fname = StringField('Surname:')
    name = StringField('Name:')
    lname = StringField('Lname:')
    b_day = StringField('Day')
    b_month = StringField('Month')
    b_year = StringField('Year')
    submit = SubmitField('Update')


class PassrecoveryForm(FlaskForm):
    email = StringField('Email:')
    password = PasswordField('Password:')
    submit = SubmitField('Recovery')
