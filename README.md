## Веб приложение на Flask

Версия Python - от 3.6.7.

### Установка зависимостей Python 

Создаем виртуальное окружение

``` python3 -m venv ./venv```

Активируем его

```source venv/bin/activate```

Устанавливаем зависимости

```pip install -r requirements.txt ```

#### Создание базы данных
```flask db upgrade```

### Запуск проекта

```flask run```

### Запуск тестов

```python -m pytest --setup-show tests```